'use strict';

import { object } from 'libcore';

// assume that angularjs is included in html using <script> tag
var Angular = global.angular;

if (!object(Angular)) {
    throw new Error('AngularJS is not loaded!');
}

export { Angular };

export default Angular;
