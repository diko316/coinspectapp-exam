'use strict';

import { jquery } from './jquery.js';

import 'foundation-sites';

import 'what-input';


global.$ = 
    global.jQuery = jquery;


jquery(global.document).foundation();


export { jquery };

export default jquery;