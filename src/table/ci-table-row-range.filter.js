'use strict';

import {
            number,
            array
        } from 'libcore';

import { ciTablePagingInfoFilterName } from './ci-table-paging-info.filter.js';

export const    ciTableRowRangeFilterName = 'ciTableRowRange',
                ciTableRowRangeFilterAnnotated = [
                                        ciTablePagingInfoFilterName + 'Filter',
                                        ciTableRowRangeFilter];


export
    function getRowsInRange(list, currentPage, itemsPerPage) {
        var info = getRowsInRange.pagingInfo(list,
                                            currentPage,
                                            itemsPerPage);

        if (array(list)) {
            return list.slice(info.from, info.to);
        }

        return [];

    }

export
    function ciTableRowRangeFilter(pagingInfo) {
        getRowsInRange.pagingInfo = pagingInfo;
        return getRowsInRange;
    }