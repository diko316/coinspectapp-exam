'use strict';


import { Angular } from '../angular.js';

import {
            ciTableColumnsFilterName,
            ciTableColumnsFilterAnnotated 
        } from './ci-table-columns.filter.js';

import {
            ciTableRowRangeFilterName,
            ciTableRowRangeFilterAnnotated
        } from './ci-table-row-range.filter.js';

import {
            ciTablePagingInfoFilterName,
            ciTablePagingInfoFilterAnnotated
        } from './ci-table-paging-info.filter.js';

import {
            ciTableFactoryName,
            ciTableFactoryAnnotated
        } from './ci-table.factory.js';

import {
            ciTableControllerName,
            ciTableControllerAnnotated
        } from './ci-table.controller.js';

import {
            ciTableGridDirectiveSelector,
            ciTableGridDirectiveAnnotated
        } from './ci-table-grid.directive.js';


import {
            ciTablePagingDirectiveName,
            ciTablePagingDirectiveAnnotated
        } from './ci-table-paging.directive.js';



export const    ciTableModule = Angular.module('ciTable', []).

                        factory(ciTableFactoryName,
                                ciTableFactoryAnnotated).
                                
                        controller(ciTableControllerName,
                                    ciTableControllerAnnotated).
                                    
                        directive(ciTableGridDirectiveSelector,
                                    ciTableGridDirectiveAnnotated).
                                    
                        directive(ciTablePagingDirectiveName,
                                    ciTablePagingDirectiveAnnotated).

                        filter(ciTableColumnsFilterName,
                                ciTableColumnsFilterAnnotated).
                        
                        filter(ciTableRowRangeFilterName,
                                ciTableRowRangeFilterAnnotated).
                                
                        filter(ciTablePagingInfoFilterName,
                                ciTablePagingInfoFilterAnnotated);

export default ciTableModule;