'use strict';

import {
            object,
            array,
            assign
        } from 'libcore';

export const    ciTableFactoryName = 'ciTableFactory',
                ciTableFactoryAnnotated = [ciTableFactory];

export
    class ciTableClass {

        constructor() {
            this.config = null;
            this.rows = [];
        }


        load(config) {
            var isValidConfig = object(config),
                currentRows = this.rows;
            var rows = null;

            this.config =
                config = isValidConfig ? config : null;

            if (isValidConfig) {
                rows = config.data;
                if (!array(rows)) {
                    rows = null;
                }
            }

            currentRows.splice(0, currentRows.length);
            currentRows.push.apply(currentRows, rows);
             
        }

        getRows() {
            return this.rows;
        }

    }


export
    function ciTableFactory(...args) {
        return new ciTableClass(...args);
    }