'use strict';

import { ciTableControllerName } from './ci-table.controller.js';

import {
            camelize,
            string
        } from 'libcore';

        //"userId": 1, "id": 3, "title": "fugiat veniam minus", "completed": false
const   TEMPLATE = `

<table class="hover unstriped">
    <thead>
        <tr>
            <th ng-repeat="column in tableControl.columns">
                <span ng-bind="column"></span>
            </th>
        </td>
    </thead>
    <tbody>
        <tr ng-repeat="record in tableControl.rows | ciTableRowRange: tableControl.page: tableControl.rowsPerPage">
            <td ng-repeat="column in tableControl.columns">
                <span ng-bind="record[column]"></span>
            </td>
        </tr>
    </tbody>
</table>
        `;

export const    ciTableGridDirectiveSelector = 'ciTableGrid',
                ciTableGridDirectiveAnnotated = ['$compile',
                                                ciTableGridDirective];

export
    class ciTableGridFactory {
        constructor($compiler) {
            this.controller = ciTableControllerName;
            this.controllerAs = 'tableControl';

            this.$compile = $compiler;

            this.gridTemplate = TEMPLATE;
            
        }

        compile(element) {

            var html = element.html();

            return {
                post: ($scope, element, attribute) => {
                    this.setupElement($scope,
                                    element,
                                    attribute,
                                    html);

                    this.postLink($scope, element, attribute);
                }
            };

        }

        setupElement($scope, element, attribute, html) {

            element.html(this.gridTemplate + ' ' + html);

            this.$compile(element.contents())($scope);

        }

        postLink($scope, element, attribute) {

            var referenceProperty = null;

            attribute.$observe('ciTableRef',
                value => {
                    var old = referenceProperty,
                        context = $scope,
                        table = context[this.controllerAs];
                    
                    if (string(value)) {

                        if (old) {
                            delete context[old];
                        }

                        referenceProperty = value = camelize(value);

                        context[value] = table;
                    }

                });

            $scope.$watch(attribute[ciTableGridDirectiveSelector],
                value => $scope.tableControl.load(value));

            $scope.$watch(attribute.ciTableColumns,
                    value => $scope.tableControl.setColumns(value));
            
        }
    }

export
    function ciTableGridDirective(...args) {
        return new ciTableGridFactory(...args);
    }
