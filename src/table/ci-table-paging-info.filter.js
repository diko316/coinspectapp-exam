'use strict';

import {
            number,
            array
        } from 'libcore';

export const    ciTablePagingInfoFilterName = 'ciTablePagingInfo',
                ciTablePagingInfoFilterAnnotated = [ciTablePagingInfoFilter];


export
    function getPagingInfo(list, currentPage, itemsPerPage) {
        var M = Math,
            isNumber = number,
            info = {
                        total: 0,
                        page: 1,
                        pages: 0,
                        itemsPerPage: 10,
                        from: 0,
                        to: 0
                    };
        var total, pages, from;

        //console.log('list ', list);

        if (array(list)) {
            info.total = 
                total = list.length;
            
            info.itemsPerPage = 
                itemsPerPage = isNumber(itemsPerPage) ?
                            M.max(0, itemsPerPage) : 10;
            info.pages = 
                pages = total && itemsPerPage > 0 ?
                            M.ceil(total / itemsPerPage) : 0;

            info.page = 
                currentPage = isNumber(currentPage) ?
                                M.max(1, currentPage) : 1;

            if (pages) {
                info.from =
                    from = (M.min(currentPage, pages) - 1) * itemsPerPage;
                
                info.to = Math.min(total,
                                from + itemsPerPage);

            }

        }

        return info;

    }

export
    function ciTablePagingInfoFilter() {
        return getPagingInfo;
    }