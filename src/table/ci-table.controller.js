'use strict';

import { ciTableFactoryName } from './ci-table.factory.js';

import { ciTableColumnsFilterName } from './ci-table-columns.filter.js';

import { ciTablePagingInfoFilterName } from './ci-table-paging-info.filter.js';

export const    ciTableControllerName = 'ciTable',
                ciTableControllerAnnotated = [ciTableFactoryName,
                                        ciTableColumnsFilterName + 'Filter',
                                        ciTablePagingInfoFilterName + 'Filter',
                                        '$scope',
                                        ciTableController];

export
    function ciTableController(TableService,
                                ColumnsFilter,
                                PagingInfoFilter,
                                $scope) {

        var vm = this;

        vm.tableService = TableService;

        vm.page = 1;
        vm.rowsPerPage = 10;
        vm.rows = [];
        vm.columns = [];

        vm.load = function (config) {
            var service = TableService,
                viewModel = vm;
            var rows, info;

            service.load(config);

            rows = service.getRows();
            info = PagingInfoFilter(viewModel.rows,
                                    viewModel.page,
                                    viewModel.rowsPerPage);

            viewModel.rows = rows;
            viewModel.page = info.page;
            
        };

        vm.setColumns = function (columns) {
            vm.columns = ColumnsFilter(columns);
        };

        vm.setPage = function (page) {
            var viewModel = vm,
                info = PagingInfoFilter(viewModel.rows,
                                        page,
                                        viewModel.rowsPerPage);

            viewModel.page = info.page;

        };

        vm.nextPage = function () {
            vm.setPage(vm.page + 1);
        };

        vm.previousPage = function () {
            vm.setPage(vm.page - 1);
        };

    }