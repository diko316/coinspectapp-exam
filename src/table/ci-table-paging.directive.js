'use strict';

import { jquery } from '../foundation.js';

import { ciTableGridDirectiveSelector } from './ci-table-grid.directive.js';

import { ciTablePagingInfoFilterName } from './ci-table-paging-info.filter.js';


const   TEMPLATE = `

<ul class="pagination">
    <li class="arrow">
        <a ng-click="table.previousPage()">&laquo;</a>
    </li>
    <li ng-repeat="page in info.pages"
        ng-class="table.page === page ? 'current' : null">
        <a ng-click="table.setPage(page)">
            <span ng-bind="page"></span>
        </a>
    </li>
    <li class="arrow">
        <a ng-click="table.nextPage()">&raquo;</a>
    </li>
</ul>


        `;

export const    ciTablePagingDirectiveName = 'ciTablePaging',
                ciTablePagingDirectiveAnnotated = [ciTablePagingDirective];

export
    class ciTablePagingFactory {
        constructor() {
            this.require = '^' + ciTableGridDirectiveSelector;
            this.template = TEMPLATE;
            this.scope = {};
        }

        link($scope, element, attribute, tableControl) {

            $scope.table = tableControl;

            $scope.info = {
                pages: []
            };

            $scope.$watchCollection('table.rows | ' +
                                    ciTablePagingInfoFilterName +
                                    ':table.page:table.rowsPerPage',
                info => {
                    var list = $scope.info.pages,
                        l = info.pages;

                    list.splice(0, list.length);

                    for (; l--;) {
                        list[l] = l + 1;
                    }

                });

        }

    }


export
    function ciTablePagingDirective(...args) {
        return new ciTablePagingFactory(...args);
    }