'use strict';

import {
            string,
            array
        } from 'libcore';

export const    ciTableColumnsFilterName = 'ciTableColumns',
                ciTableColumnsFilterAnnotated = [ciTableColumnsFilter];


export
    function getValidColumns(list) {
        var isString = string;
        var l;

        if (array(list)) {
            list = list.slice(0);

            for (l = list.length; l--;) {
                if (!isString(list[l])) {
                    list.splice(l, 1);
                }
            }

            return list;
        }

        return null;
    }

export
    function ciTableColumnsFilter() {
        return getValidColumns;
    }