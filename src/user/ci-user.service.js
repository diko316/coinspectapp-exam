'use strict';

import appConfig from '../config.json';

import {
            array,
            object,
            clone
        } from 'libcore';


export const    ciUserServiceName = "ciUsers",
                ciUserServiceAnnotated = ['$http',
                                            '$q',
                                            '$rootScope',
                                            ciUserService];

export
    function ciUserService(...args) {
        return new ciUserModel(...args);
    }


export
    class ciUserModel {

        constructor($http, $q, $root) {
            this.Promise = $q;
            this.http = $http;
            this.rootScope = $root;

            this.httpConfig = {
                url: appConfig.dataResource + '/users',
                method: 'GET',
                withCredentials: true
            };

            this.rows = null;
            this.list();
            
        }

        list() {
            var config = clone(this.httpConfig, true);

            return this.http(config).
                        then((data) => {
                            this.rows = data;
                            this.rootScope.$applyAsync();
                            return data;
                        },
                        (error) => {
                            console.warn('http error: ', error);
                            this.rows = null;
                            throw new Error(error);
                        });

        }

        getRows() {
            var data = this.rows;
            var rows;

            if (object(data)) {
                rows = data.data;
                if (array(rows)) {
                    return rows;
                }
            }
            return null;
        }

        get(id) {
            var isObject = object,
                rows = this.getRows();
            var l, row;

            if (rows) {
                for (l = rows.length; l--;) {
                    row = rows[l];
                    if (isObject(row) && row.id === id) {
                        return row;
                    }
                }
            }

            return null;
        }
    }

