'use strict';


import { Angular } from '../angular.js';

import {
            ciUserServiceName,
            ciUserServiceAnnotated
        } from './ci-user.service.js';

export const    ciUserModule = Angular.module('ciUser', []).
                        service(ciUserServiceName,
                                ciUserServiceAnnotated);

export default ciUserModule;
