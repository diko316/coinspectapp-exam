'use strict';


import { ciTodoCompletedRecordsFilterName  }
             from './ci-todo-completed-records.filter.js';

import { ciTodoNotCompletedRecordsFilterName  }
             from './ci-todo-not-completed-records.filter.js';


export const    ciTodoStateRecordsFilterName = 'ciTodoStateRecords',
                ciTodoStateRecordsFilterAnnotated = [
                                ciTodoCompletedRecordsFilterName + 'Filter',
                                ciTodoNotCompletedRecordsFilterName + 'Filter',
                                ciTodoStateRecordsFilter];

export
    function filterByType(items, type) {

        switch (type) {
        case 'completed': return filterByType.$completeFilter(items);

        case 'incomplete': return filterByType.$incompleteFilter(items);
        
        }

        return items;
    }

export
    function ciTodoStateRecordsFilter(completeFilter, incompleteFilter) {
        filterByType.$completeFilter = completeFilter;
        filterByType.$incompleteFilter = incompleteFilter;

        return filterByType;
    }