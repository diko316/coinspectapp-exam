'use strict';

import appConfig from '../config.json';

import {
            array,
            object,
            contains,
            string,
            number,
            clone
        } from 'libcore';


export const    ciTodoServiceName = "ciTodo",
                ciTodoServiceAnnotated = ['$http',
                                            '$q',
                                            '$rootScope',
                                            ciTodoService];

export
    function ciTodoService(...args) {
        return new ciTodoModel(...args);
    }


export
    class ciTodoModel {

        constructor($http, $q, $root) {
            this.Promise = $q;
            this.http = $http;
            this.rootScope = $root;

            this.httpConfig = {
                url: appConfig.dataResource + '/todos',
                method: 'GET',
                withCredentials: true
            };

            this.rows = null;
            this.list();
            
        }

        list() {
            var config = clone(this.httpConfig, true);

            return this.http(config).
                            then((data) => {
                                this.rows = data;
                                this.rootScope.$applyAsync();
                                return data;
                            },
                            (error) => {
                                console.warn('http error: ', error);
                                this.rows = null;
                                throw new Error(error);
                            });

        }

        joinUsers(userRows) {
            var todos = this.rows,
                isArray = array,
                isObject = object,
                isString = string,
                isNumber = number,
                has = contains,
                userRecordIndex = {},
                result = [],
                len = 0;
            var userId, c, l, record;

            if (isArray(userRows) && isObject(todos) &&
                isArray(todos = todos.data)) {
                
                // index users
                for (c = -1, l = userRows.length; l--;) {
                    record = userRows[++c];
                    if (isObject(record)) {
                        userId = record.id;
                        if (isNumber(userId) || isString(userId)) {
                            userRecordIndex[userId] = record;
                        }
                    }
                }

                // recreate todo records with user object
                for (c = -1, l = todos.length; l--;) {
                    record = todos[++c];

                    if (isObject(record)) {
                        userId = record.userId;

                        if (isString(userId) || isNumber(userId)) {

                            if (has(userRecordIndex, userId)) {
                                record = clone(record, true);
                                record.user = userRecordIndex[userId];
                                result[len++] = record;
                            }
                            
                        }
                        
                    }
                }
            }

            return result;
        }
    }

