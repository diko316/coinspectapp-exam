'use strict';

import { ciTodoServiceName } from './ci-todo.service.js';

import {
            object,
            array
        } from 'libcore';

import { ciUserServiceName } from '../user/ci-user.service.js';

export const    ciTodoControllerName = 'ciTodo',
                ciTodoControllerAnnotated = [ciTodoServiceName,
                                                '$scope',
                                                '$filter',
                                                ciUserServiceName,
                                                ciTodoController];


export
    function ciTodoController(service, $scope, $filter, users) {

        var vm = this;

        vm.users = users;

        vm.data = 
            vm.withUsers =
            vm.rows = null;

        vm.getUser = function (id) {
            return users.get(id);
        };

        service.list().
            then(data => {
                var rows;

                data = object(data) ? data : null;
                rows = data && data.data;
                
                vm.data = data;
                vm.rows = array(rows) ? rows : null;

                $scope.$applyAsync();
                
            });

        $scope.$watchCollection(() => users.getRows(),
            value => {
                vm.withUsers = object(value) ?
                                        service.joinUsers(value) : null;
            });

        $scope.$watchCollection(() => vm.rows,
            () => {
                vm.withUsers = service.joinUsers(users.getRows());
            });


    }