'use strict';


import { Angular } from '../angular.js';

import { ciUserModule } from '../user/module.js';

import {
            ciTodoServiceName,
            ciTodoServiceAnnotated
        } from './ci-todo.service.js';

import {
            ciTodoControllerName,
            ciTodoControllerAnnotated
        } from './ci-todo.controller.js';

import {
            ciTodoCompletedRecordsFilterName,
            ciTodoCompletedRecordsFilterAnnotated
        } from './ci-todo-completed-records.filter.js';

import {
            ciTodoCompletedCountDirectiveSelector,
            ciTodoCompletedCountDirectiveAnnotated
        } from './ci-todo-completed-count.directive.js';


import {
            ciTodoNotCompletedRecordsFilterName,
            ciTodoNotCompletedRecordsFilterAnnotated
        } from './ci-todo-not-completed-records.filter.js';


import {
            ciTodoStateRecordsFilterName,
            ciTodoStateRecordsFilterAnnotated
        } from './ci-todo-state-records.filter.js';


import {
            ciTodoNotCompletedCountDirectiveSelector,
            ciTodoNotCompletedCountDirectiveAnnotated
        } from './ci-todo-not-completed-count.directive.js';



export const    modules = [ciUserModule.name],
                ciTodoModule = Angular.module('ciTodo', modules).

                    service(ciTodoServiceName,
                            ciTodoServiceAnnotated).

                    controller(ciTodoControllerName,
                                ciTodoControllerAnnotated).

                    directive(ciTodoCompletedCountDirectiveSelector,
                        ciTodoCompletedCountDirectiveAnnotated).

                    filter(ciTodoCompletedRecordsFilterName,
                        ciTodoCompletedRecordsFilterAnnotated).


                    directive(ciTodoNotCompletedCountDirectiveSelector,
                            ciTodoNotCompletedCountDirectiveAnnotated).
                        
                    filter(ciTodoNotCompletedRecordsFilterName,
                            ciTodoNotCompletedRecordsFilterAnnotated).
                            
                    filter(ciTodoStateRecordsFilterName,
                            ciTodoStateRecordsFilterAnnotated);

export default ciTodoModule;
