'use strict';


import { ciTodoControllerName } from './ci-todo.controller.js';

export const    ciTodoNotCompletedCountDirectiveSelector = 'ciTodoNotCompletedCount',
                ciTodoNotCompletedCountDirectiveAnnotated = [
                                    ciTodoNotCompletedCountDirective];

export
    class ciTodoNotCompletedCountFactory {
        constructor() {
            this.controller = ciTodoControllerName;
            this.controllerAs = 'todo';
            this.scope = {};
            this.template = `{{ (todo.rows | ciTodoNotCompletedRecords).length }}`;

        }

        link($scope, element, attribute) {
        }
    }

export
    function ciTodoNotCompletedCountDirective(...args) {
        return new ciTodoNotCompletedCountFactory(...args);
    }