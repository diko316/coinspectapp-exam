'use strict';


import { ciTodoControllerName }
                            from './ci-todo.controller.js';


export const    ciTodoCompletedCountDirectiveSelector = 'ciTodoCompletedCount',
                ciTodoCompletedCountDirectiveAnnotated = [
                                    ciTodoCompletedCountDirective];

export
    class ciTodoCompletedCountFactory {
        constructor() {
            this.controller = ciTodoControllerName;
            this.controllerAs = 'todo';
            this.template = `
            {{ (todo.rows | ciTodoCompletedRecords).length }}
            `;
            this.scope = {};
        }

        link($scope, element, attribute) {
        }
    }

export
    function ciTodoCompletedCountDirective(...args) {
        return new ciTodoCompletedCountFactory(...args);
    }