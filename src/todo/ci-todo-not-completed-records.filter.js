'use strict';


import {
            object,
            array
        } from 'libcore';

function getNotCompletedTodo(records) {
    var isObject = object,
        completed = [];

    var c, l, record, len;

    if (array(records)) {
        len = 0;
        for (c = -1, l = records.length; l--;) {
            record = records[++c];
            if (isObject(record) && record.completed === false) {
                completed[len++] = record;
            }
        }
    }

    return completed;

}

export const    ciTodoNotCompletedRecordsFilterName = 'ciTodoNotCompletedRecords',
                ciTodoNotCompletedRecordsFilterAnnotated = [
                                            ciTodoNotCompletedRecordsFilter];

export
    function ciTodoNotCompletedRecordsFilter() {

        return getNotCompletedTodo;

    }