'use strict';


import {
            object,
            array
        } from 'libcore';

function getCompletedTodo(records) {
    var isObject = object,
        completed = [];

    var c, l, record, len;

    if (array(records)) {
        len = 0;
        for (c = -1, l = records.length; l--;) {
            record = records[++c];
            if (isObject(record) && record.completed === true) {
                completed[len++] = record;
            }
        }
    }

    return completed;

}

export const    ciTodoCompletedRecordsFilterName = 'ciTodoCompletedRecords',
                ciTodoCompletedRecordsFilterAnnotated = [
                                                ciTodoCompletedRecordsFilter];

export
    function ciTodoCompletedRecordsFilter() {

        return getCompletedTodo;

    }