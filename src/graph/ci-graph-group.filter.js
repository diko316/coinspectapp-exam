'use strict';

import {
            string,
            array,
            object,
            contains
        } from 'libcore';

export const    ciGraphGroupFilterName = 'ciGraphGroup',
                ciGraphGroupFilterAnnotated = [ciGraphGroupFilter];

export
    function groupBy(rows, columnName) {
        var has = contains,
            isObject = object,
            groups = [],
            len = 0;

        var c, l, row, cell, gl, groupCell, found;

        if (!string(columnName)) {
            columnName = null;
        }

        if (array(rows)) {

            for (c = -1, l = rows.length; l--;) {
                row = rows[++c];

                if (isObject(row)) {
                    cell = columnName && has(row, columnName) ?
                                row[columnName] : null;
                    found = null;

                    group: for (gl = groups.length; gl--;) {
                        groupCell = groups[gl];
                        if (groupCell.value === cell) {
                            found = groupCell;
                            break group;
                        }
                    }

                    if (!found) {
                        groups[len++] = found = {
                            value: cell,
                            rows: []
                        };
                    }

                    found = found.rows;
                    found[found.length] = row;
                    
                }
            }
        }

        return groups;

    }

export
    function ciGraphGroupFilter() {
        return groupBy;
    }