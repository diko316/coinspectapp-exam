'use strict';

import {
            array,
            object
        } from 'libcore';


export const    ciGraphAggregateCountFilterName = 'ciGraphGroupCount',
                ciGraphAggregateCountFilterAnnotated = [
                                                ciGraphAggregateCountFilter];

export  
    function countGroups(groups) {
        var isArray = array,
            isObject = object,
            result = [],
            len = 0;
        var l, group, rows, count;

        if (isArray(groups)) {
            for (l = groups.length; l--;) {
                group = groups[l];
                count = 0;
                if (isObject(group)) {
                    rows = group.rows;
                    count = isArray(rows) ? rows.length : 0;
                }
                
                result[len++] = {
                    value: count,
                    group: group
                };
            }
        }

        return result;
    }

export
    function ciGraphAggregateCountFilter() {
        return countGroups;
    }