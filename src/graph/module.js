'use strict';

import { Angular } from '../angular.js';

import { ciTableModule } from '../table/module.js';

import {
            ciGraphGroupFilterName,
            ciGraphGroupFilterAnnotated
        } from './ci-graph-group.filter.js';

import {
            ciGraphAggregateCountFilterName,
            ciGraphAggregateCountFilterAnnotated
        } from './ci-graph-aggregate-count.js';

import {
            ciBarGraphValueRangeFilterName,
            ciBarGraphValueRangeFilterAnnotated
        } from './ci-bar-graph-value-range.filter.js';


import {
            ciGraphFactoryName,
            ciGraphFactoryAnnotated
        } from './ci-graph.factory.js';

import {
            ciGraphControllerName,
            ciGraphControllerAnnotated
        } from './ci-graph.controller.js';

import {
            ciBarGraphDirectiveSelector,
            ciBarGraphDirectiveAnnotated
        } from './ci-bar-graph.directive.js';

export const    modules = [ciTableModule.name],
                ciGraphModule = Angular.module('ciGraph', modules).

                    filter(ciGraphGroupFilterName,
                            ciGraphGroupFilterAnnotated).

                    filter(ciGraphAggregateCountFilterName,
                            ciGraphAggregateCountFilterAnnotated).

                    filter(ciBarGraphValueRangeFilterName,
                            ciBarGraphValueRangeFilterAnnotated).

                    factory(ciGraphFactoryName,
                            ciGraphFactoryAnnotated).

                    controller(ciGraphControllerName,
                                ciGraphControllerAnnotated).

                    directive(ciBarGraphDirectiveSelector,
                                ciBarGraphDirectiveAnnotated);

