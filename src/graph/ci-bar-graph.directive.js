'use strict';

import { ciGraphControllerName } from './ci-graph.controller.js';

const   TEMPLATE = `
    <ul class="bar-graph">
        <li class="bar-graph-axis">
            <div class="bar-graph-label"
                ng-repeat="item in graphControl.yData">
                <span ng-bind="item"></span>
            </div>
        </li>
        <li class="bar primary"
            title="0"
            ng-attr-title="{{ group.value }}"
            ng-style="{ height: graphControl.formatPercentXRecord(group) + '%' }"
            ng-repeat="group in graphControl.aggregated">
            <div class="percent">
                <span ng-bind="graphControl.formatPercentXRecord(group) + '%'"></span>
            </div>
            <div class="description"
                ng-init="$group = group.group">
                {DISPLAY}
            </div>
        </li>
    </ul>
`;

export const    ciBarGraphDirectiveSelector = 'ciBarGraph',
                ciBarGraphDirectiveAnnotated = ['$filter',
                                                '$compile',
                                                ciBarGraphDirective];

export
    class ciBarGraphFactory {
        constructor($filter, $compile) {
            this.controller = ciGraphControllerName;
            this.controllerAs = 'graphControl';

            this.$filter = $filter;
            this.$compile = $compile;
        }

        compile(element) {
            var html = element.html();

            element.html('');

            return {
                post: ($scope, element, attribute) => {
                    this.setupHTML($scope, element, html);
                    this.postLink($scope, element, attribute);
                }
            };
        }

        setupHTML($scope, element, html) {

            var template = TEMPLATE.replace(/\{DISPLAY\}/g, html);

            element.html(template);

            this.$compile(element.contents())($scope);

        }

        postLink($scope, element, attribute) {

            var control = $scope.graphControl;

            $scope.$graphControl = control;
            
            $scope.$watchCollection(attribute[ciBarGraphDirectiveSelector],
                value => {
                    control.load(value);
                });

            $scope.$watch(attribute.ciBarGroup,
                    value => control.groupBy(value));

            $scope.$watch(attribute.ciBarGraphAggregate,
                    value => control.setAggregate(value));

            attribute.$observe('ciBarGraphYDisplay',
                    value => control.setYGroupDisplayExpr(value));
        }

    }

export
    function ciBarGraphDirective(...args) {
        return new ciBarGraphFactory(...args);
    }