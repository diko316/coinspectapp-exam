'use strict';

import { ciTableClass } from '../table/ci-table.factory.js';

import { array } from 'libcore';

export const    ciGraphFactoryName = 'ciGraphFactory',
                ciGraphFactoryAnnotated = [ciGraphFactory];



export
    class ciGraphClass extends ciTableClass {

        
        load(config) {
            if (array(config)) {
                config = { data: config };
            }
            
            return super.load(config);
        }
    }


export
    function ciGraphFactory(...args) {
        return new ciGraphClass(...args);
    }