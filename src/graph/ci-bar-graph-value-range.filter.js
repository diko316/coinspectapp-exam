'use strict';

import {
            array,
            object,
            number
        } from 'libcore';

export const    ciBarGraphValueRangeFilterName = 'ciBarGraphValueRange',
                ciBarGraphValueRangeFilterAnnotated = [
                                                ciBarGraphValueRangeFilter];

export
    function getAggregatedValueRange(aggregated) {
        var M = Math,
            isObject = object,
            isNumber = number,
            maxValue = 0;

        var l, item, value;

        if (array(aggregated)) {
            for (l = aggregated.length; l--;) {
                item = aggregated[l];
                if (isObject(item) && isNumber(value = item.value)) {
                    maxValue = M.max(maxValue, value);
                }
            }
        }

        return maxValue ? 
                [
                    maxValue,
                    (maxValue / 2).toFixed(2) * 1,
                    0,
                ] :
                [0];
    }


export
    function ciBarGraphValueRangeFilter() {
        return getAggregatedValueRange;
    }