'use strict';

import {
            string,
            object,
            array,
            number
        } from 'libcore';

import { ciGraphFactoryName } from './ci-graph.factory.js';

import { ciGraphGroupFilterName } from './ci-graph-group.filter.js';

import { ciBarGraphValueRangeFilterName }
                                from './ci-bar-graph-value-range.filter.js';

export const    ciGraphControllerName = 'ciGraph',
                ciGraphControllerAnnotated = [ciGraphFactoryName,
                                            '$filter',
                                            '$scope',
                                            ciGraphGroupFilterName + 'Filter',
                                            ciGraphController];

export
    function ciGraphController(graphFactory, $filter, $scope, groupFilter) {
        var vm = this,
            yFilter = $filter(ciBarGraphValueRangeFilterName);

        vm.groups = [];

        vm.groupByColumn = null;

        vm.aggregate = null;
        vm.aggregated = null;

        vm.yData = null;
        vm.xDisplayExpr = null;

        vm.load = function (config) {

            graphFactory.load(config);

            $scope.$applyAsync();
        };

        vm.groupBy = function (column) {
            var aggregate = vm.aggregate;

            var groups;

            vm.groupByColumn = column;
            vm.groups =
                groups = groupFilter(graphFactory.rows, column);

            vm.aggregated = aggregate ? aggregate(groups) : null;
            vm.yData = yFilter(vm.aggregated);

            $scope.$applyAsync();

        };

        vm.getMaxYData = function () {
            var data = vm.yData;
            var value;

            if (array(data) && data.length &&
                number(value = data[0])) {
                return value;
            }

            return 0;
        };


        vm.setAggregate = function(value) {
            var filter = null;

            try {
                filter = $filter(value);
            }
            catch (e) {}

            vm.aggregate = filter;
            vm.aggregated = filter ? filter(vm.groups) : null;

            // update x data
            vm.yData = yFilter(vm.aggregated);

            $scope.$applyAsync();
            
        };

        vm.setYGroupDisplayExpr = function (value) {
            vm.xDisplayExpr = string(value) ? value : null;
            $scope.$applyAsync();
        };

        vm.formatDisplayXRecord = function (record) {
            var expr = vm.xDisplayExpr;

            if (object(record) && string(expr)) {

                return $scope.$eval(expr, {
                            "$record": record
                        });
            }
            return '';
        };

        vm.formatPercentXRecord = function (record) {
            var max = vm.getMaxYData(),
                percent = 0;
            var value;

            if (max && object(record) && number(value = record.value)) {
                return Math.round(value / max * 100);
            }

            return percent;

        };

        $scope.$watchCollection(() => graphFactory.rows,
            () => vm.groupBy(vm.groupByColumn));


    }