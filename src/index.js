'use strict';


import './foundation.js';

import { Angular } from './angular.js';

import { ciTableModule } from './table/module.js';

import { ciGraphModule } from './graph/module.js';

import { ciTodoModule } from './todo/module.js';

import { ciUserModule } from './user/module.js';


//console.log('jquery? ', jquery);


const   modules = [ciTodoModule.name,
                    ciTableModule.name,
                    ciGraphModule.name,
                    ciUserModule.name],
        ciFrontEndExam = Angular.module('ciFrontEndExam', modules);

export { ciFrontEndExam };

export default ciFrontEndExam;


